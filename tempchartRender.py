from scipy.interpolate import make_interp_spline, BSpline
from matplotlib.ticker import MaxNLocator
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from threading import Thread
import numpy as np
import datetime
import time
import math
import os

def time_align():

    #time align if time is not 00 min
    while True:
        if '0' in datetime.datetime.now().strftime('%M')[1] and '00' in datetime.datetime.now().strftime('%S'):
            break  
        time.sleep(1)  
    
def run_temp():

    #first array at start
    temp = float(os.popen("vcgencmd measure_temp").readline().replace('temp=', '')[0:4])
    i=0
    array = np.array([])
    while i<=30:
        array = np.append(array, temp)
        i+=1
    return array

def measurement(array):

    #measurement
    temp = float(os.popen("vcgencmd measure_temp").readline().replace('temp=', '')[0:4])
    array = array[1:]
    array = np.append(array, temp)
    return array

def chart(array):

    #chart script
    #array
    x = np.arange(0, 6.2, 0.2)

    #smooth
    xnew = np.linspace(x.min(), x.max(), 300) 
    spl = make_interp_spline(x, array, k=3)
    power_smooth = spl(xnew)

    #font
    plt.rcParams["font.family"] = "Franklin Gothic Medium"

    #new figure
    fig = plt.figure(1, figsize=(8,4))
    chart = fig.add_subplot(111)

    #plot
    plot, = chart.plot(xnew, power_smooth, color='#7289DA', markeredgecolor='#3498DB')
   
    #temp avg/min/max
    min_temp = min(array)
    max_temp = max(array)
    avg_temp = round(sum(array)/len(array),1)
    temp_value = "MAX: {:>10}°C\nMIN:  {:>10}°C\nAVG:  {:>10}°C".format(max_temp,min_temp,avg_temp)

    #legend
    tmpval = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0, animated=True)
    legend = plt.legend((plot,tmpval), ('Temperature (°C)', temp_value),loc=4, facecolor="#2F3136", framealpha=0.8, edgecolor='#7289DA', handlelength=3)
    plt.setp(legend.get_texts(), color='#FFFFFF')


    #chart max temperature scale
    if array.max() >= 50:
        tickmax = 60
    else:
        tickmax = 50

    #customization
    plt.tight_layout()
    chart.set_facecolor('#2F3136')
    chart.grid(True, color='#23272A', axis='y')
    chart.spines["bottom"].set_color("white")
    chart.spines["left"].set_color("#2f3136")
    chart.spines["right"].set_color("#2f3136")
    chart.spines["top"].set_color("#2f3136")
    chart.axis([x[0], x[-1], 30, tickmax])
    chart.yaxis.set_major_locator(MaxNLocator(integer=True))
    chart.xaxis.set_major_locator(MaxNLocator(integer=True))
    chart.fill_between(xnew, power_smooth, alpha=0.3, color='#7289DA')

    #ticks & labels params
    chart.tick_params(axis='x', colors="white", length=10.0)
    chart.tick_params(axis='y', colors='#2F3136', labelcolor='#FFFFFF', length=2.0)
    
    #time labels
    currentHour = datetime.datetime.now()
    labels = [item.get_text() for item in chart.get_xticklabels()]
    labels[0] = (currentHour - datetime.timedelta(hours=1)).strftime('%H:%M')
    labels[1] = (currentHour - datetime.timedelta(minutes=50)).strftime('%H:%M')
    labels[2] = (currentHour - datetime.timedelta(minutes=40)).strftime('%H:%M')
    labels[3] = (currentHour - datetime.timedelta(minutes=30)).strftime('%H:%M')
    labels[4] = (currentHour - datetime.timedelta(minutes=20)).strftime('%H:%M')
    labels[5] = (currentHour - datetime.timedelta(minutes=10)).strftime('%H:%M')
    labels[6] = currentHour.strftime('%H:%M')
    chart.set_xticklabels(labels)

    #save chart and delete old
    plt.savefig('tempchart.png', bbox_inches='tight', facecolor='#2F3136')
    fig.clear()
    
def main():
    
    #main program
    array = run_temp()
    Thread(target=chart, args=(array,)).start()
    time_align()  
    while True:
        Thread(target=chart, args=(array,)).start()
        i=0
        while i<5:
            array = measurement(array)
            i += 1
            time.sleep(120)
        if not '0' in datetime.datetime.now().strftime('%M')[1]:
            time_align()

if __name__ == '__main__':
    main()
